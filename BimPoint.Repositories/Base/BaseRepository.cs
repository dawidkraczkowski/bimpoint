﻿using BimPoint.Repositories.Context;
using BimPoint.Repositories.Interfaces.Base;
using BimPoint.Repositories.Models.Base;
using System.Collections.Generic;
using System.Linq;

namespace BimPoint.Repositories.Base
{
    public abstract class BaseRepository : IBaseRepository
    {
        protected readonly DatabaseContext _context;

        public BaseRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }

    public abstract class BaseRepository<TModel> : BaseRepository, IBaseRepository<TModel>
        where TModel : BaseModel
    {
        public BaseRepository(DatabaseContext context) : base(context)
        {
        }

        public virtual void Add(TModel item)
        {
            _context.Set<TModel>().Add(item);
        }

        public virtual void Add(IEnumerable<TModel> items)
        {
            _context.Set<TModel>().AddRange(items);
        }

        public virtual IQueryable<TModel> Get()
        {
            return _context.Set<TModel>();
        }

        public virtual void Remove(TModel item)
        {
            _context.Set<TModel>().Remove(item);
        }

        public virtual void Remove(IEnumerable<TModel> items)
        {
            _context.Set<TModel>().RemoveRange(items);
        }
    }
}