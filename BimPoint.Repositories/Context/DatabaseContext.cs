﻿using BimPoint.Repositories.Context.TypeConfigurations;
using BimPoint.Repositories.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BimPoint.Repositories.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public virtual DbSet<Assignment> Assignments { get; set; }

        public void Seed()
        {
            if (!Assignments.Any())
            {
                Assignments.Add(new Assignment()
                {
                    Title = "First task",
                    Description = "This is first task added every time database is created."
                });

                Assignments.Add(new Assignment()
                {
                    Title = "Second task",
                    Description = "This is second task added every time database is created."
                });

                Assignments.Add(new Assignment()
                {
                    Title = "Third task",
                    Description = "This is third task added every time database is created."
                });

                SaveChanges();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AssignmentTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}