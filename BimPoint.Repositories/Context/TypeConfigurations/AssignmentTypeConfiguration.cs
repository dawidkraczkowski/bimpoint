﻿using BimPoint.Repositories.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BimPoint.Repositories.Context.TypeConfigurations
{
    public class AssignmentTypeConfiguration : IEntityTypeConfiguration<Assignment>
    {
        public void Configure(EntityTypeBuilder<Assignment> builder)
        {
            builder.ToTable("Assignments")
                .HasKey(x => x.Id);
        }
    }
}