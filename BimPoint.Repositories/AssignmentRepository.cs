﻿using BimPoint.Repositories.Base;
using BimPoint.Repositories.Context;
using BimPoint.Repositories.Interfaces;
using BimPoint.Repositories.Models;

namespace BimPoint.Repositories
{
    public class AssignmentRepository : BaseRepository<Assignment>, IAssignmentRepository
    {
        public AssignmentRepository(DatabaseContext context) : base(context)
        {
        }
    }
}