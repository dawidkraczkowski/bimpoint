﻿using BimPoint.Services.Interfaces.Base;
using BimPoint.Services.Models;
using System.Collections.Generic;

namespace BimPoint.Services.Interfaces
{
    public interface IAssignmentService : IBaseService
    {
        int? CreateAssignment(Assignment assignment);

        bool DeleteAssignment(int id);

        Assignment GetAssignmentById(int id);

        IList<Assignment> GetAssignments();

        bool UpdateAssignment(Assignment assignment);
    }
}