using BimPoint.Repositories.Interfaces;
using BimPoint.Services.Interfaces;
using BimPoint.Services.Tests.Base;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace BimPoint.Services.Tests
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AssignmentServiceTests : BaseTestFixture<IAssignmentService>
    {
        private static readonly object[] CreateAssignmentTestSource =
        {
            new object[] { 0, "title 1", "description 1" },
            new object[] { 0, "title 2", "description 2" }
        };

        private static readonly object[] GetAssignmentByIdTest_EmptySource =
        {
            new object[] { 1 },
            new object[] { 5 }
        };

        private static readonly object[] GetAssignmentByIdTest_NotPresentSource =
        {
            new object[] { 15 },
            new object[] { 6 }
        };

        private static readonly object[] GetAssignmentByIdTestSource =
        {
            new object[] { 1 },
            new object[] { 5 }
        };

        private static readonly object[] GetAssignmentsTestSource =
        {
            new object[] { 0 },
            new object[] { 1 },
            new object[] { 5 }
        };

        private Mock<IAssignmentRepository> _assignmentRepository;
        private IList<Repositories.Models.Assignment> _drones;

        [TestCaseSource(nameof(CreateAssignmentTestSource))]
        public void CreateAssignmentTest(int id, string title, string description)
        {
            // Arrange
            // Act
            Service.CreateAssignment(new Models.Assignment() { Id = id, Title = title, Description = description });

            // Assert
            _assignmentRepository.Verify(x => x.Add(It.Is<Repositories.Models.Assignment>(y => y.Title == title && y.Description == description)), Times.Once);
            _assignmentRepository.Verify(x => x.SaveChanges(), Times.Once);

            Assert.AreEqual(1, _drones.Count(x => x.Id != 0));
        }

        [TestCaseSource(nameof(GetAssignmentByIdTestSource))]
        public void GetAssignmentByIdTest(int id)
        {
            // Arrange
            for (int i = 1; i <= 5; i++)
            {
                _drones.Add(new Repositories.Models.Assignment() { Id = i, Title = "title", Description = "Description" });
            }

            // Act
            var result = Service.GetAssignmentById(id);

            // Assert
            _assignmentRepository.Verify(x => x.Get(), Times.Once);

            Assert.NotNull(result);
        }

        [TestCaseSource(nameof(GetAssignmentByIdTest_EmptySource))]
        public void GetAssignmentByIdTest_Empty(int id)
        {
            // Arrange
            // Act
            var result = Service.GetAssignmentById(id);

            // Assert
            _assignmentRepository.Verify(x => x.Get(), Times.Once);

            Assert.Null(result);
        }

        [TestCaseSource(nameof(GetAssignmentByIdTest_NotPresentSource))]
        public void GetAssignmentByIdTest_NotPresent(int id)
        {
            // Arrange
            for (int i = 1; i <= 5; i++)
            {
                _drones.Add(new Repositories.Models.Assignment() { Id = i, Title = "title", Description = "Description" });
            }

            // Act
            var result = Service.GetAssignmentById(id);

            // Assert
            _assignmentRepository.Verify(x => x.Get(), Times.Once);

            Assert.Null(result);
        }

        [TestCaseSource(nameof(GetAssignmentsTestSource))]
        public void GetAssignmentsTest(int count)
        {
            // Arrange
            for (int i = 1; i <= count; i++)
            {
                _drones.Add(new Repositories.Models.Assignment() { Id = i, Title = "title", Description = "Description" });
            }

            // Act
            var result = Service.GetAssignments();

            // Assert
            _assignmentRepository.Verify(x => x.Get(), Times.Once);

            Assert.NotNull(result);
            Assert.AreEqual(count, result.Count);
        }

        protected override void SetUp()
        {
            base.SetUp();

            int initialDroneId = 0;
            _drones = new List<Repositories.Models.Assignment>();

            _assignmentRepository = new Mock<IAssignmentRepository>();
            _assignmentRepository.Setup(x => x.Get()).Returns(_drones.AsQueryable());
            _assignmentRepository.Setup(x => x.Add(It.IsAny<Repositories.Models.Assignment>()))
                .Callback<Repositories.Models.Assignment>(x =>
                {
                    _drones.Add(x);
                });
            _assignmentRepository.Setup(x => x.SaveChanges())
                .Callback(() =>
                {
                    foreach (var drone in _drones.Where(x => x.Id == 0))
                    {
                        drone.Id = ++initialDroneId;
                    }
                });

            Service = new AssignmentService(_assignmentRepository.Object);
        }
    }
}