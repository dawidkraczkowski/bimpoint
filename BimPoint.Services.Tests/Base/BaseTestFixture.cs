﻿using NUnit.Framework;
using System.Diagnostics.CodeAnalysis;

namespace BimPoint.Services.Tests.Base
{
    [ExcludeFromCodeCoverage]
    public class BaseTestFixture<TService>
    {
        protected TService Service;

        [SetUp]
        public void ExecuteSetUp()
        {
            SetUp();
        }

        [TearDown]
        public void ExecuteTearDown()
        {
            TearDown();
        }

        protected virtual void SetUp()
        {
        }

        protected virtual void TearDown()
        {
        }
    }
}