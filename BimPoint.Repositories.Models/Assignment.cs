﻿using BimPoint.Repositories.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace BimPoint.Repositories.Models
{
    public class Assignment : BaseModel<int>
    {
        public Assignment()
        {
        }

        #region Identifiers

        #endregion Identifiers

        #region Properties

        public string Description { get; set; }

        [Required]
        public string Title { get; set; }

        #endregion Properties

        #region Navigation properties

        #endregion Navigation properties
    }
}