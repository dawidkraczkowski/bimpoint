﻿namespace BimPoint.Repositories.Models.Base
{
    public abstract class BaseModel
    {
        #region Identifiers

        #endregion Identifiers

        #region Properties

        #endregion Properties

        #region Navigation properties

        #endregion Navigation properties
    }

    public abstract class BaseModel<TKey> : BaseModel
    {
        #region Identifiers

        public TKey Id { get; set; }

        #endregion Identifiers

        #region Properties

        #endregion Properties

        #region Navigation properties

        #endregion Navigation properties
    }
}