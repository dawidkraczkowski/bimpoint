﻿using BimPoint.Repositories.Models.Base;
using System.Collections.Generic;
using System.Linq;

namespace BimPoint.Repositories.Interfaces.Base
{
    public interface IBaseRepository
    {
        void SaveChanges();
    }

    public interface IBaseRepository<TModel> : IBaseRepository
        where TModel : BaseModel
    {
        void Add(TModel item);

        void Add(IEnumerable<TModel> items);

        IQueryable<TModel> Get();

        void Remove(TModel item);

        void Remove(IEnumerable<TModel> items);
    }
}