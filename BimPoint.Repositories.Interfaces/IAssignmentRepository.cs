﻿using BimPoint.Repositories.Interfaces.Base;
using BimPoint.Repositories.Models;

namespace BimPoint.Repositories.Interfaces
{
    public interface IAssignmentRepository : IBaseRepository<Assignment>
    {
    }
}