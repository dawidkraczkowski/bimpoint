﻿using BimPoint.Areas.UI.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace BimPoint.Areas.UI.ViewModels.Home
{
    public class AssignmentCreateModalPartialViewModel : BaseViewModel
    {
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
    }
}