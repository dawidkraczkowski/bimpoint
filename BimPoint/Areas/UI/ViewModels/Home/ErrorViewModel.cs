using BimPoint.Areas.UI.ViewModels.Base;

namespace BimPoint.Areas.UI.ViewModels.Home
{
    public class ErrorViewModel : BaseViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId
        {
            get { return !string.IsNullOrEmpty(RequestId); }
        }
    }
}