﻿using BimPoint.Areas.UI.ViewModels.Base;

namespace BimPoint.Areas.UI.ViewModels.Home
{
    public class AssignmentElementPartialViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}