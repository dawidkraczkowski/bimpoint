﻿using BimPoint.Areas.UI.Controllers.Base;
using BimPoint.Areas.UI.ViewModels.Home;
using BimPoint.Constants.Areas.UI;
using BimPoint.Services.Interfaces;
using BimPoint.Services.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace BimPoint.Areas.UI.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IAssignmentService _assignmentService = null;

        public HomeController(IAssignmentService assignmentService)
        {
            _assignmentService = assignmentService;
        }

        [ActionName(HomeConstants.Actions.AssignmentCreateModalPartial)]
        public IActionResult AssignmentCreateModalPartial()
        {
            return PartialView(HomeConstants.Views.AssignmentCreateModalPartial, new AssignmentCreateModalPartialViewModel()
            {
                Title = string.Empty,
                Description = string.Empty
            });
        }

        [ActionName(HomeConstants.Actions.AssignmentElement)]
        public IActionResult AssignmentElement(int id)
        {
            var assignment = _assignmentService.GetAssignmentById(id);

            return RedirectToAction(HomeConstants.Actions.AssignmentElementPartial, new { id, title = assignment.Title });
        }

        [ActionName(HomeConstants.Actions.AssignmentElementPartial)]
        public IActionResult AssignmentElementPartial(int id, string title)
        {
            return PartialView(HomeConstants.Views.AssignmentElementPartial, new AssignmentElementPartialViewModel()
            {
                Id = id,
                Title = title
            });
        }

        [ActionName(HomeConstants.Actions.AssignmentUpdateModal)]
        public IActionResult AssignmentUpdateModal(int id)
        {
            var assignment = _assignmentService.GetAssignmentById(id);

            return RedirectToAction(HomeConstants.Actions.AssignmentUpdateModalPartial, new { id, title = assignment.Title, description = assignment.Description });
        }

        [ActionName(HomeConstants.Actions.AssignmentUpdateModalPartial)]
        public IActionResult AssignmentUpdateModalPartial(int id, string title, string description)
        {
            return PartialView(HomeConstants.Views.AssignmentUpdateModalPartial, new AssignmentUpdateModalPartialViewModel()
            {
                Id = id,
                Title = title,
                Description = description
            });
        }

        [HttpPost]
        [ActionName(HomeConstants.Actions.CreateAssignment)]
        public IActionResult CreateAssignment(AssignmentCreateModalPartialViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(null);

            var id = _assignmentService.CreateAssignment(new Assignment()
            {
                Description = model.Description,
                Title = model.Title
            });

            return Json(id);
        }

        [HttpDelete]
        [ActionName(HomeConstants.Actions.DeleteAssignment)]
        public IActionResult DeleteAssignment(int id)
        {
            var isDeleted = _assignmentService.DeleteAssignment(id);

            return Json(isDeleted);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [ActionName(HomeConstants.Actions.Error)]
        public IActionResult Error()
        {
            return View(SharedConstants.Views.Error, new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [ActionName(HomeConstants.Actions.Index)]
        public IActionResult Index()
        {
            IList<AssignmentElementPartialViewModel> model = _assignmentService.GetAssignments()
                .Select(x => new AssignmentElementPartialViewModel()
                {
                    Id = x.Id,
                    Title = x.Title
                }).ToList();

            return View(HomeConstants.Views.Index, model);
        }

        [HttpPost]
        [ActionName(HomeConstants.Actions.UpdateAssignment)]
        public IActionResult UpdateAssignment(AssignmentUpdateModalPartialViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(null);

            var isUpdated = _assignmentService.UpdateAssignment(new Assignment()
            {
                Id = model.Id,
                Description = model.Description,
                Title = model.Title
            });

            return Json(isUpdated);
        }
    }
}