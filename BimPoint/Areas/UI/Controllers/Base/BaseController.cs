﻿using BimPoint.Constants.Areas;
using Microsoft.AspNetCore.Mvc;

namespace BimPoint.Areas.UI.Controllers.Base
{
    [Area(AreaConstants.UI)]
    public abstract class BaseController : Controller
    {
    }
}