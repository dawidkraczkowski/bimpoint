﻿using BimPoint.Constants.Areas;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BimPoint.Areas.API.Controllers.Base
{
    [Area(AreaConstants.API)]
    [Produces("application/json")]
    public abstract class BaseController : ControllerBase
    {
        protected IActionResult Result<T>(T result, int statusCode = StatusCodes.Status200OK)
        {
            return new ObjectResult(result)
            {
                StatusCode = statusCode
            };
        }

        protected IActionResult Result(int statusCode = StatusCodes.Status200OK)
        {
            return new StatusCodeResult(statusCode);
        }
    }
}