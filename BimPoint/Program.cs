﻿using BimPoint.Constants;
using BimPoint.Repositories.Context;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BimPoint
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Setup environment
            EnvironmentConfiguration.Initialize();

            IWebHost webHost = WebHost.CreateDefaultBuilder(args).UseStartup<Startup>().Build();

            using (var scope = webHost.Services.CreateScope())
            {
                try
                {
                    DatabaseContext context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();

                    if (scope.ServiceProvider.GetService<IConfiguration>().GetSection("RecreateDatabase").Get<bool>())
                        context.Database.EnsureDeleted();

                    context.Database.Migrate();

                    context.Seed();
                }
                catch
                {
                }
            }

            webHost.Run();
        }
    }
}