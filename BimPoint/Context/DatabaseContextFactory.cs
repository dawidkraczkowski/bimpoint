﻿using BimPoint.Constants;
using BimPoint.Repositories.Base;
using BimPoint.Repositories.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace BimPoint.Context
{
    public class DatabaseContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            EnvironmentConfiguration.Initialize();

            // Build config
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../BimPoint"))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{EnvironmentConfiguration.Current.Name}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            var builder = new DbContextOptionsBuilder<DatabaseContext>();

            builder.UseSqlite(configuration.GetConnectionString("DefaultConnection"), x => x.MigrationsAssembly(typeof(BaseRepository).Assembly.FullName));

            return new DatabaseContext(builder.Options);
        }
    }
}