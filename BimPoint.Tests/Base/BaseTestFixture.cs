﻿using NUnit.Framework;
using System.Diagnostics.CodeAnalysis;

namespace BimPoint.Tests.Base
{
    [ExcludeFromCodeCoverage]
    public class BaseTestFixture<TController>
    {
        protected TController Controller;

        [SetUp]
        public void ExecuteSetUp()
        {
            SetUp();
        }

        [TearDown]
        public void ExecuteTearDown()
        {
            TearDown();
        }

        protected virtual void SetUp()
        {
        }

        protected virtual void TearDown()
        {
        }
    }
}