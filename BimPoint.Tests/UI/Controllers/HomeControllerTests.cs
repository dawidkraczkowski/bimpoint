﻿using BimPoint.Areas.UI.Controllers;
using BimPoint.Areas.UI.ViewModels.Home;
using BimPoint.Services.Interfaces;
using BimPoint.Tests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Diagnostics.CodeAnalysis;

namespace BimPoint.Tests.UI.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class HomeControllerTests : BaseTestFixture<HomeController>
    {
        private static readonly object[] AssignmentElementPartialTestSource =
        {
            new object[] { 15, "first title" },
            new object[] { 6, "second title" }
        };

        private static readonly object[] AssignmentElementTestSource =
        {
            new object[] { 15 },
            new object[] { 6 }
        };

        private Mock<IAssignmentService> _assignmentService;

        [TestCaseSource(nameof(AssignmentElementPartialTestSource))]
        public void AssignmentElementPartialTest(int id, string title)
        {
            // Arrange
            // Act
            var result = Controller.AssignmentElementPartial(id, title) as PartialViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(Constants.Areas.UI.HomeConstants.Views.AssignmentElementPartial, result.ViewName);
            Assert.True(result.Model is AssignmentElementPartialViewModel);
            Assert.AreEqual(id, (result.Model as AssignmentElementPartialViewModel).Id);
            Assert.AreEqual(title, (result.Model as AssignmentElementPartialViewModel).Title);
        }

        [TestCaseSource(nameof(AssignmentElementTestSource))]
        public void AssignmentElementTest(int id)
        {
            // Arrange

            _assignmentService.Setup(x => x.GetAssignmentById(id)).Returns(new Services.Models.Assignment()
            {
                Id = id,
                Title = "assignment title",
                Description = "description"
            });

            // Act
            var result = Controller.AssignmentElement(id) as RedirectToActionResult;

            // Assert
            _assignmentService.Verify(x => x.GetAssignmentById(id), Times.Once);

            Assert.NotNull(result);
            Assert.AreEqual(Constants.Areas.UI.HomeConstants.Actions.AssignmentElementPartial, result.ActionName);
            Assert.True(result.RouteValues.Keys.Contains("id"));
            Assert.True(result.RouteValues.Keys.Contains("title"));
            Assert.AreEqual(id, result.RouteValues["id"]);
            Assert.AreEqual("assignment title", result.RouteValues["title"]);
        }

        protected override void SetUp()
        {
            base.SetUp();

            _assignmentService = new Mock<IAssignmentService>();

            Controller = new HomeController(_assignmentService.Object);
        }
    }
}