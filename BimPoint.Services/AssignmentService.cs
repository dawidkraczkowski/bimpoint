﻿using BimPoint.Repositories.Interfaces;
using BimPoint.Services.Base;
using BimPoint.Services.Interfaces;
using BimPoint.Services.Mapping;
using BimPoint.Services.Models;
using System.Collections.Generic;
using System.Linq;

namespace BimPoint.Services
{
    public class AssignmentService : BaseService, IAssignmentService
    {
        private readonly IAssignmentRepository _assignmentRepository = null;

        public AssignmentService(IAssignmentRepository assignmentRepository)
        {
            _assignmentRepository = assignmentRepository;
        }

        public int? CreateAssignment(Assignment assignment)
        {
            try
            {
                Repositories.Models.Assignment repositoryAssignment = new Repositories.Models.Assignment();

                ModelMapper.Map(assignment, repositoryAssignment);

                _assignmentRepository.Add(repositoryAssignment);
                _assignmentRepository.SaveChanges();

                return repositoryAssignment.Id;
            }
            catch
            {
                return null;
            }
        }

        public bool DeleteAssignment(int id)
        {
            try
            {
                Repositories.Models.Assignment repositoryAssignment = _assignmentRepository.Get()
                .FirstOrDefault(x => x.Id == id);

                if (repositoryAssignment == null)
                    return false;

                _assignmentRepository.Remove(repositoryAssignment);
                _assignmentRepository.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Assignment GetAssignmentById(int id)
        {
            Repositories.Models.Assignment repositoryAssignment = _assignmentRepository.Get()
                .FirstOrDefault(x => x.Id == id);

            return ModelMapper.Map<Repositories.Models.Assignment, Assignment>(repositoryAssignment);
        }

        public IList<Assignment> GetAssignments()
        {
            IList<Repositories.Models.Assignment> repositoryAssignments = _assignmentRepository.Get()
                    .ToList();

            return ModelMapper.Map<IList<Repositories.Models.Assignment>, IList<Assignment>>(repositoryAssignments);
        }

        public bool UpdateAssignment(Assignment assignment)
        {
            try
            {
                Repositories.Models.Assignment repositoryAssignment = _assignmentRepository.Get()
                .FirstOrDefault(x => x.Id == assignment.Id);

                ModelMapper.Map(assignment, repositoryAssignment);

                _assignmentRepository.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}