﻿using AutoMapper;

namespace BimPoint.Services.Mapping
{
    internal static class ModelMapper
    {
        static ModelMapper()
        {
            Instance = Configuration.Instance.CreateMapper();
        }

        public static IMapper Instance { get; }

        public static TDestination Map<TSource, TDestination>(TSource source)
        {
            return Instance.Map<TSource, TDestination>(source);
        }

        public static TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return Instance.Map(source, destination);
        }
    }
}