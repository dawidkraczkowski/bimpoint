﻿using AutoMapper;

namespace BimPoint.Services.Mapping.Profiles
{
    internal class ServiceToRepository : Profile
    {
        public ServiceToRepository()
        {
            CreateMap<Services.Models.Assignment, Repositories.Models.Assignment>()
                .PreserveReferences();
        }
    }
}