﻿using AutoMapper;

namespace BimPoint.Services.Mapping.Profiles
{
    internal class RepositoryToService : Profile
    {
        public RepositoryToService()
        {
            CreateMap<Repositories.Models.Assignment, Services.Models.Assignment>()
                .PreserveReferences();
        }
    }
}