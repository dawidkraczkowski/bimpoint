﻿using AutoMapper;
using AutoMapper.Configuration;
using BimPoint.Services.Mapping.Profiles;

namespace BimPoint.Services.Mapping
{
    internal class Configuration : MapperConfiguration
    {
        private static MapperConfigurationExpression ConfigurationExpression = null;

        static Configuration()
        {
            ConfigurationExpression = new MapperConfigurationExpression();

            ConfigurationExpression.AddProfile<RepositoryToService>();
            ConfigurationExpression.AddProfile<ServiceToRepository>();

            Mapper.Initialize(ConfigurationExpression);
            Mapper.AssertConfigurationIsValid();

            Instance = new Configuration();
        }

        private Configuration() : base(ConfigurationExpression)
        {
        }

        public static Configuration Instance { get; }
    }
}