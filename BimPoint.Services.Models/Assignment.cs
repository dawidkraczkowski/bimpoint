﻿using BimPoint.Services.Models.Base;

namespace BimPoint.Services.Models
{
    public class Assignment : BaseModel<int>
    {
        public Assignment()
        {
        }

        #region Identifiers

        #endregion Identifiers

        #region Properties

        public string Description { get; set; }

        public string Title { get; set; }

        #endregion Properties

        #region Navigation properties

        #endregion Navigation properties
    }
}