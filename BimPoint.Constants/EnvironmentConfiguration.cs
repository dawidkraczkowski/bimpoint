﻿using System;

namespace BimPoint.Constants
{
    public static class EnvironmentConfiguration
    {
        static EnvironmentConfiguration()
        {
            Development = new Configuration("Development");
            Production = new Configuration("Production");
        }

        public static Configuration Current { get; private set; }

        #region Configurations

        public static Configuration Development { get; }
        public static Configuration Production { get; }

        #endregion Configurations

        public static void Initialize()
        {
#if DEBUG
            Current = Development;
#else
            Current = Production;
#endif

            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", Current.Name, EnvironmentVariableTarget.Process);
        }

        public class Configuration
        {
            public Configuration(string name)
            {
                Name = name;
            }

            public string Name { get; }
        }
    }
}