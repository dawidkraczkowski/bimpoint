﻿namespace BimPoint.Constants.Areas
{
    public static class AreaConstants
    {
        public const string API = "API";
        public const string UI = "UI";
    }
}