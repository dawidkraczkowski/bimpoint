﻿namespace BimPoint.Constants.Areas.UI
{
    public static class SharedConstants
    {
        public static class Views
        {
            public const string _Layout = "/Areas/UI/Views/Shared/_Layout.cshtml";
            public const string _ValidationScriptsPartial = "/Areas/UI/Views/Shared/_ValidationScriptsPartial.cshtml";
            public const string Error = "/Areas/UI/Views/Shared/Error.cshtml";
        }
    }
}