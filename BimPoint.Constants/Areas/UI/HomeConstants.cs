﻿namespace BimPoint.Constants.Areas.UI
{
    public static class HomeConstants
    {
        public const string ControllerName = "Home";

        public static class Actions
        {
            public const string AssignmentCreateModalPartial = "AssignmentCreateModalPartial";
            public const string AssignmentElement = "AssignmentElement";
            public const string AssignmentElementPartial = "AssignmentElementPartial";
            public const string AssignmentUpdateModal = "AssignmentUpdateModal";
            public const string AssignmentUpdateModalPartial = "AssignmentUpdateModalPartial";
            public const string CreateAssignment = "CreateAssignment";
            public const string DeleteAssignment = "DeleteAssignment";
            public const string Error = "Error";
            public const string Index = "Index";
            public const string UpdateAssignment = "UpdateAssignment";
        }

        public static class Views
        {
            public const string AssignmentCreateModalPartial = "/Areas/UI/Views/Home/AssignmentCreateModalPartial.cshtml";
            public const string AssignmentElementPartial = "/Areas/UI/Views/Home/AssignmentElementPartial.cshtml";
            public const string AssignmentUpdateModalPartial = "/Areas/UI/Views/Home/AssignmentUpdateModalPartial.cshtml";
            public const string Index = "/Areas/UI/Views/Home/Index.cshtml";
        }
    }
}